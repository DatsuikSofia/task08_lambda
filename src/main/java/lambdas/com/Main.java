package lambdas.com;
import pattern.com.Contex;
import pattern.com.LowerCase;
import pattern.com.UpperCase;

import static java.lang.Math.*;
import static java.lang.StrictMath.log;

public class Main {
    public static void main(String[] args) {
        Triplet max = (a, b, c) -> Math.max(a, Math.max(b, c));
        Triplet average = (a, b, c) -> (a + b + c) / 3;
        System.out.println(max.calculate(1, 11, 5));
        System.out.println(average.calculate(4, 7, 10));
        Contex contex = new Contex("sofia " ,new LowerCase());
        contex.doAction();
        contex.setState(new UpperCase());
        contex.doAction();


    }
}


