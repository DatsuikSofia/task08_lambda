package lambdas.com;

@FunctionalInterface
public interface Triplet {
    int calculate(int a, int b, int c);
}
