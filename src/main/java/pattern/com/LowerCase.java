package pattern.com;

public class  LowerCase implements  State{
    @Override
    public void doAction(Contex contex) {
        System.out.println(contex.name.toLowerCase());
    }

}
