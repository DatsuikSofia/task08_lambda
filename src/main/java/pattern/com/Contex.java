package pattern.com;

/*interface  State {
    void  doAction( Contex contex );

}
/*class  LowerCase implements  State{
    @Override
    public void doAction(Contex contex) {
        System.out.println(contex.name.toLowerCase());
    }
}*/
/*class  UpperCase implements  State{
    @Override
    public void doAction(Contex contex) {
        System.out.println(contex.name.toUpperCase());

    }
}*/


public  class Contex {
    State state ;
    String name;

    public Contex(String name , State state) {
        this.name = name;
        this.state = state;
    }
    public void setState(State state) {
        this.state = state;
    }
    public void doAction(){
        state.doAction(this);
    }
}

