package pattern.com;

public class  UpperCase implements  State{
    @Override
    public void doAction(Contex contex) {
        System.out.println(contex.name.toUpperCase());

    }
}
